#!/usr/bin/python

import pymysql

class DB_CONNECTION_TEST:

    def __init__(self, database, user, password, host, port,charset):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.charset = charset
    
    def connect_db_test(self):
                
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        print("Trying connect postgres database ... ")
        try:
            conn = pymysql.connect(**db_settings)
            print("Database connection successfully")
            
        except:
            print("Database connection fail")
        return conn

    
class MYSQL_TABLE_HISTORY:
    
    def __init__(self, database, user, password, host, port,charset):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.charset = charset
        
    def select_all_data(self):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """select * from table_history"""
            cursor.execute(sql)
            result = cursor.fetchall()
            return result
        except (Exception, pymysql.Error) as error:
            print("Failed to select record: ", error)
            return False
        finally:
            cursor.close()
            connection.close()
            
    def select_data_tablename(self, str_tablename):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """select * from table_history where table_name=\'{:s}\';"""
            sql_formate = sql.format(str_tablename)
            cursor.execute(sql_formate)
            result = cursor.fetchone()
            return result
        except (Exception, pymysql.Error) as error:
            print("Failed to select record where table name is " + str_tablename, error)
            return False
        finally:
            cursor.close()
            connection.close()
            
    def insert_data(self, str_tablename, str_starttime, str_endtime):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """INSERT INTO table_history (table_name,start_time,end_time) VALUES (\'{:s}\',\'{:s}\',\'{:s}\');"""
            sql_format = sql.format(str_tablename, str_starttime, str_endtime)
            cursor.execute(sql_format)
            connection.commit()
            return True
        except:
            print("Failed to insert data into MySQL, table name: " + str_tablename , error)
            return False
        finally:
            cursor.close()
            connection.close()
            
    def update_data_starttime(self,  str_tablename, str_starttime):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """UPDATE table_history SET start_time=\'{:s}\' WHERE table_name=\'{:s}\';""";
            sql_format = sql.format(str_starttime,str_tablename)
            cursor.execute(sql_format)
            connection.commit()
            return True
        except:
            print("Failed to update data into MySQL, table name: " + str_tablename , error)
            return False
        finally:
            cursor.close()
            connection.close()
    
    def update_data_endtime(self,  str_tablename, str_endtime):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """UPDATE table_history SET end_time=\'{:s}\' WHERE table_name=\'{:s}\';""";
            sql_format = sql.format(str_endtime,str_tablename)
            cursor.execute(sql_format)
            connection.commit()
            return True
        except:
            print("Failed to update data into MySQL, table name: " + str_tablename , error)
            return False
        finally:
            cursor.close()
            connection.close()
            
    def update_data(self,  str_tablename, str_starttime, str_endtime):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """UPDATE table_history SET start_time=\'{:s}\',end_time=\'{:s}\' WHERE table_name=\'{:s}\';""";
            sql_format = sql.format(str_starttime, str_endtime, str_tablename)
            cursor.execute(sql_format)
            connection.commit()
            return True
        except:
            print("Failed to update data into MySQL, table name: " + str_tablename , error)
            return False
        finally:
            cursor.close()
            connection.close()
    
    def delete_data(self, str_tablename):
        db_settings = {
                "host": self.host,
                "port": self.port,
                "user": self.user,
                "password": self.password,
                "db": self.database,
                "charset": self.charset
            }
        try:
            connection = pymysql.connect(**db_settings)
            cursor = connection.cursor()
            sql = """DELETE FROM table_history WHERE table_name=\'{:s}\';""";
            sql_format = sql.format(str_tablename)
            cursor.execute(sql_format)
            connection.commit()
            return True
        except:
            print("Failed to delete data into MySQL, table name: " + str_tablename , error)
            return False
        finally:
            cursor.close()
            connection.close()
    