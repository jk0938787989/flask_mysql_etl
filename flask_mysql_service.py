#!/usr/bin/python

import datetime
from datetime import timedelta
import time
import os
import json
import pymysql
import mysql_dbconnection
import pandas as pd
from flask import Flask, Response, request, jsonify, abort, make_response
#from flask_cors import CORS

app = Flask(__name__)
app.config["DEBUG"] = False
#cors = CORS(app, resource={r"/.*":{"origins":"*"}})

'etl_history','root','cathaylife','34.133.66.103',5702,'utf8'

# Mysql database infomation

db_info = {
    'dbname':'etl_history',
    'username':'root',
    'password':'cathaylife',
    'ip':'34.133.66.103',
    'port':5702,
    'charset':'utf8'
}

# def time_compare(start_time, end_time): # False: 開始時間大於結束時間，代表正在進行ETL
#     start_dt = datetime.datetime.strptime(start_time, "%Y/%m/%d %H:%M:%S")
#     end_dt = datetime.datetime.strptime(end_time, "%Y/%m/%d %H:%M:%S")
#     if start_dt >= end_dt:
#         return False
#     else:
#         return True
    
def time_calculator(start_time, end_time): # 計算起訖時間差，設定門檻值 K
    
    start_dt = datetime.datetime.strptime(start_time, "%Y/%m/%d %H:%M:%S")
    end_dt = datetime.datetime.strptime(end_time, "%Y/%m/%d %H:%M:%S")
    now_time = datetime.datetime.now()
    diff_start_end = end_dt - start_dt
    diff_end_now = now_time - end_dt
    k = 0
    if diff_start_end <= timedelta(hours=24):
        k = 24
    else:
        k = 48
    if diff_end_now <= timedelta(hours=k): # 小於 K 值，不做；大於 K 值，做
        return False
    else:
        return True
    
def query_table(tablename):
    conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
    res = conn.select_data_tablename(tablename)
    return res
    
@app.route('/test', methods=['GET'])
def home():
    return "<h1>Hello Mysql Service Flask!</h1>"

@app.route('/get_table_status', methods=['GET'])
def get_table_status():
    conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
    db_service_info = conn.select_all_data()
    service_json = []
    service_key = ["table_name", "start_time", "end_time"]
    for item in db_service_info:
        list_service_value = list(item)
        sub_service_dict = dict(zip(service_key, list_service_value))
        service_json.append(sub_service_dict)
    return jsonify(service_json)

@app.route('/return_etl_status', methods=['POST','GET'])
def return_etl_status(): # 0: True, do ETL ; 1: False, do not do ETL 
    request_data = request.get_json()
    query_result = query_table(request_data['table_name'])
    if query_result != None:
        if query_result[2] == 'error':
            now_time = (datetime.datetime.now()).strftime("%Y/%m/%d %H:%M:%S")
            conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
            db_service_info = conn.update_data(request_data['table_name'],now_time,'running')
            print("[SYSTEM] ETL Start, table name : " + str(request_data['table_name']))
            return '0'
        elif query_result[2] == 'running':
            print("[SYSTEM] ETL Fail, {:s} is running etl ... ".format(request_data['table_name']))
            return '1'
        else:
            k_time = time_calculator(query_result[1],query_result[2])
            if k_time:
                now_time = (datetime.datetime.now()).strftime("%Y/%m/%d %H:%M:%S")
                conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
                db_service_info = conn.update_data(request_data['table_name'],now_time,'running')
                if db_service_info:
                    print("[SYSTEM] ETL Start, table name : " + str(request_data['table_name']))
                    return '0'
                else:
                    print("[SYSTEM] ETL error, table {:s} occur db update error.".format(request_data['table_name']))
                    return '1'
            else:
                print("[SYSTEM] ETL Fail, {:s} just etl in 24/48 hours ... ".format(request_data['table_name']))
                return '1'
    else:
        now_time = (datetime.datetime.now()).strftime("%Y/%m/%d %H:%M:%S")
        conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
        db_service_info = conn.insert_data(request_data['table_name'],now_time,'running')
        print("[SYSTEM] New table. ETL Start, table name : " + str(request_data['table_name']))
        return '0'
    
@app.route('/done_successful_etl', methods=['POST'])
def done_successful_etl():
    request_data = request.get_json()
    now_time = (datetime.datetime.now()).strftime("%Y/%m/%d %H:%M:%S")
    conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
    db_service_info = conn.update_data_endtime(request_data['table_name'],now_time)
    return "[SYSTEM] ETL Successful."
    
@app.route('/done_fail_etl', methods=['POST'])
def done_fail_etl():
    request_data = request.get_json()
    conn = mysql_dbconnection.MYSQL_TABLE_HISTORY(db_info['dbname'], db_info['username'], db_info['password'], db_info['ip'], db_info['port'],  db_info['charset'])
    db_service_info = conn.update_data_endtime(request_data['table_name'],'error')
    return "[SYSTEM] ETL Fail."

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5566)
    
